import React from "react";

class counter extends React.Component {
  constructor() {
    super();
    this.state = { count: 0 };
  }
  render() {
    return (
      <div className="container">
        {this.state.count}
        <hr />
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          +1
        </button>
        <button onClick={() => this.setState({ count: this.state.count - 1 })}>
          -1
        </button>
        <button onClick={() => this.setState({ count: 0 })}>reset</button>
      </div>
    );
  }
}

export default counter;
