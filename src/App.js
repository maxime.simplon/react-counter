import React from "react";
import Info from "./components/info";
import Counter from "./components/counter";
import Footer from "./components/footer";
import "./App.css";

const App = () => 
  <div className="App-header">
    <div className="App">
      <h1>{Info.title}</h1>
      <p>{Info.techno}</p>
      <hr />
      <Counter />
    </div>
    <Footer />
  </div>

export default App;
