# react-counter

I made a very simple counter app using React.

It can be useful to understand the basics of React:

- import/export, components, JSX and ES6 syntax 

I hosted the app on Netlify, you can see it on https://reactcounter.netlify.com/

### Instructions : 

- clone the repository
- $ cd react-counter
- $ npm install 
- $ npm start 

